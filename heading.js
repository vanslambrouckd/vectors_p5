function setup() {
  createCanvas(300,300);
}

function draw() {
  background(0);
  strokeWeight(14);
  stroke(255,0,0);

  var sourceVec = createVector(0,0);
  point(sourceVec.x, sourceVec.y);

  var destVec = createVector(mouseX, mouseY);
  point(destVec.x, destVec.y);

  strokeWeight(1);
  stroke(255);
  line(sourceVec.x, sourceVec.y, destVec.x, destVec.y);

  var angle = destVec.heading(); //berekent de angle van de vector vanaf de origin van de canvas (0,0);
  var deg = degrees(angle);

  noStroke();
  fill(255);
  text("angle: " + deg.toFixed(0) + "°", width-80, 20);
}
